#!/bin/sh
set -e
_basedir="$(dirname "$(readlink -f "${0}")")"
cd "$_basedir"
if [ ! -d "$_basedir"/ceres-icewm/ceres-pro/icons ]; then
	rm -rf "$_basedir"/ceres-icewm/ceres-pro/icons
	cd "$_basedir"/ceres-icewm/ceres-pro/
	sh .build-icons.sh
	cd "$_basedir"
fi
LANG=C
MESSAGE="$(date -u '+%Y%m%d%H%M%S')"
cp -f packaging/debian/debian/changelog_template packaging/debian/debian/changelog
sed -i s'/__COMMIT__/'$MESSAGE'/g' packaging/debian/debian/changelog
cp -f packaging/suse/.buildsuserpm.sh.template packaging/suse/buildsuserpm.sh
sed -i s'/__COMMIT__/'$MESSAGE'/g' packaging/suse/buildsuserpm.sh

#building of packages
#disabled since packages are to big now to publish them on git!
#if type dh_testdir >/dev/null 2>&1; then
#	cd "$_basedir"/packaging
#	sh build_deb.sh
#	mv ceres-theme-icewm_2.*_all.deb ceres-icewm-current.deb
#fi
#if type rpmbuild >/dev/null 2>&1; then
#	cd "$_basedir"/packaging
#	sh build_rpm.sh
#	mv ceres-theme-icewm-2.*.noarch.rpm ceres-icewm-current.rpm
#fi

cd "$_basedir"
git add .
git commit -m "$MESSAGE"
git push origin master
printf "\n...done\n"
exit 0
