#!/bin/bash
set -e
_dir=`dirname "$(readlink -f "${0}")"`
_basedir=${_dir}
cd ${_basedir}
if [ -f ${_basedir}/../ceres-theme-icewm*.rpm ]; then
    rm -f ${_basedir}/../ceres-theme-icewm*.rpm
fi
if [ -f ${_basedir}/../ceres-current.rpm ]; then
    rm -f ${_basedir}/../ceres-current.rpm
fi
if [ -f ${_basedir}/ceres-theme-icewm.spec ]; then
    rm -f ${_basedir}/ceres-theme-icewm.spec
fi
if [ -d ${_basedir}/ceres-theme-icewm ]; then
    rm -rf ${_basedir}/ceres-theme-icewm
fi
#icewm
	mkdir -p ${_basedir}/ceres-theme-icewm/usr/share/icewm/themes
	cp -R ${_basedir}/../../ceres-icewm/* ${_basedir}/ceres-theme-icewm/usr/share/icewm/themes
##### REMOVE unwanted files
	rm -f ${_basedir}/ceres-theme-icewm/usr/share/icewm/themes/ceres-pro/.build-icons.sh

#creating the spec file:
cat <<\EOFALL> ${_basedir}/ceres-theme-icewm.spec
Buildroot: BUILDROOT
Name: ceres-theme-icewm
Version: 2.20210221140659
Release: 1
Summary: Ceres icewm theme
License: BSD 2 Clause
Requires: icewm, google-noto-fonts, noto-mono-fonts
Provides: ceres-theme-icewm = %version
Obsoletes: ceres-theme-icewm <= %version
Recommends: iceWMCP, iceWMCP-addons, icecc, menumaker, compton, xsettingsd
Group: System/X11
BuildArch: noarch

%define _rpmdir ../
%define _unpackaged_files_terminate_build 0
%define _source_payload w0.gzdio
%define _binary_payload w0.gzdio
%description
Ceres theme

%post
#set icon
_distro=$(grep -oP '(?<=^ID=).+' /etc/os-release|tr -d '"')
if [ -f /usr/share/icewm/themes/ceres-pro/taskbar/start_${_distro}.png ]; then
	ln -fs start_${_distro}.png /usr/share/icewm/themes/ceres-pro/taskbar/start.png
	ln -fs start_${_distro}.png /usr/share/icewm/themes/ceres-pro@2/taskbar/start.png
fi

%postun

%files
%defattr(-,root,root,-)
#%doc COPYING ReadMe.md LICENSE
%{_datadir}/*
EOFALL

rpmbuild -bb --buildroot=${_basedir}/ceres-theme-icewm ${_basedir}/ceres-theme-icewm.spec
if [ -f ${_basedir}/../noarch/ceres-theme-icewm*.rpm ]; then
	mv ${_basedir}/../noarch/ceres-theme-icewm*.rpm ${_basedir}/..
	rm -rf ${_basedir}/../noarch ${_basedir}/ceres-theme-icewm.spec
fi
