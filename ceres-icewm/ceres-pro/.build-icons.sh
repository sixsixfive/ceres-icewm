#!/bin/sh
set -e
(command -v sed &>/dev/null||(printf '\nGNU SED is not installed!\n' && exit 1))
(command -v rsvg-convert &>/dev/null||(printf '\nRSVG-CONVERT is not installed!\n' && exit 1))
(command -v optipng &>/dev/null||(printf '\nOPTIPNG is not installed!\n' && exit 1))
(command -v find &>/dev/null||(printf '\nFIND is not installed!\n' && exit 1))
(command -v git &>/dev/null||(printf '\nGIT is not installed!\n' && exit 1))
(command -v readlink &>/dev/null||(printf '\nREADLINK is not installed!\n' && exit 1))
_basedir="$(dirname "$(readlink -f "${0}")")"
cd "$_basedir"
if [ -d "$_basedir"/icons ]; then
  rm -rf "$_basedir"/icons
fi
cd /tmp
if [ -d /tmp/DarK-icons ]; then
	rm -rf /tmp/DarK-icons
fi
git clone --depth 1 https://gitlab.com/sixsixfive/DarK-icons.git && cd DarK-icons
cp -R DarK-SRC/scalable/pool icewmbuilddir
cd icewmbuilddir
printf "\ncreating pngs!\n"
for _f in $(find -type f -name '*.svg'); do 
  rsvg-convert -a -h 16 -f png -o ''$(echo $_f|sed 's#\.svg#\_16x16.png#')'' ''$_f''
  rsvg-convert -a -h 32 -f png -o ''$(echo $_f|sed 's#\.svg#\_32x32.png#')'' ''$_f''
  rm -f $_f
done
printf "\nrelinking symlinks!\n"
for _svgsymlink in $(find -type l -name '*.svg'); do 
  ln -sf "$(readlink $_svgsymlink|sed 's#.svg$#_16x16.png#')" "$(ls $_svgsymlink|sed 's#.svg$#_16x16.png#')"
  ln -sf "$(readlink $_svgsymlink|sed 's#.svg$#_32x32.png#')" "$(ls $_svgsymlink|sed 's#.svg$#_32x32.png#')"
  rm -f $_svgsymlink
done
printf "\noptimize images!\n"
for _f in $(find -type f); do 
  optipng -clobber -silent -o7 -nb -strip all $_f
done
if [ ! -d $_basedir/icons ]; then
  mkdir -p $_basedir/icons
fi
cd $_basedir
mv -f /tmp/DarK-icons/icewmbuilddir/*.png $_basedir/icons
printf "\n...done\n"
exit 0
